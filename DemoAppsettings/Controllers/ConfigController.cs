﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace DemoAppsettings.Controllers
{

	[Route("[controller]")]
	public class ConfigController : Controller
	{
		private readonly Config _config;

		public ConfigController(Config config)
		{
			_config = config;
		}
		public IActionResult Get()
		{
			return Ok(_config.MyConStr);
		}
	}
	public class Config
	{
		public string MyConStr { get; set; }
	}
}
